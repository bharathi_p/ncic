﻿using System;
using System.Linq;
using DAL;
using NCICObjects;
using System.IO;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;

namespace BAL
{
    public class UserAccount
    {
        public bool CreateLogin(LoginObject oLoginObject)
        {
            NCICDataContext oNCICDataContext = new NCICDataContext();

            Login oLogin = ConvertToLogin(oLoginObject, null);
            oNCICDataContext.Logins.InsertOnSubmit(oLogin);
            oNCICDataContext.SubmitChanges(); 
            int Userid = oLogin.ID;
            if (Userid != 0)
            {
                oLoginObject.ID = Userid;
                User oUser = ConvertToUser(oLoginObject, null);
                oNCICDataContext.Users.InsertOnSubmit(oUser);
                oNCICDataContext.SubmitChanges(); 
            } 
            return true;
        }

        private User ConvertToUser(LoginObject oLoginObject, User oUser)
        {
            if (oUser == null)
            {
                oUser = new User();
            }
            oUser.ID = oLoginObject.ID;
            oUser.Name = oLoginObject.UserName;
            oUser.EmailId = oLoginObject.EmailId;
            oUser.ContactNo = oLoginObject.ContactNo;
            if (oLoginObject.CreatedAt != DateTime.MinValue)
            {
                oUser.CreatedAt = oLoginObject.CreatedAt;
                oUser.CreatedBy = oLoginObject.CreatedBy;
                oUser.Status = "A";
            }
            oUser.ModifiedAt = oLoginObject.ModifiedAt;
            oUser.ModifiedBy = oLoginObject.ModifiedBy;

            return oUser;
        }
        private Login ConvertToLogin(LoginObject oLoginObject, Login oLogin)
        {
            if (oLogin == null)
            {
                oLogin = new Login();
            }
            oLogin.UserName = oLoginObject.EmailId;
            oLogin.Password = Hashing(oLoginObject.Password);
            return oLogin;
        }

        public LoginObject UserLoginDetails(string EmailId, string Password)
        {
            NCICDataContext oNCICDataContext = new NCICDataContext();
            var oCurentUser = (from oLogin in oNCICDataContext.Logins
                               join usr in oNCICDataContext.Users on oLogin.ID equals usr.ID
                               where oLogin.UserName == EmailId && oLogin.Password == Hashing(Password)
                               select new
                               {
                                   usr.EmailId,
                                   usr.Name
                               }).FirstOrDefault();
            LoginObject oLoginObject = null;
            if (oCurentUser != null && oCurentUser.EmailId != null)
            {
                oLoginObject = new LoginObject();
                oLoginObject.EmailId = oCurentUser.EmailId;
                oLoginObject.UserName = oCurentUser.Name;
            }
            return oLoginObject;
        }

        private string Hashing(string Password)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(Password);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }

            return hashString;
        }


    }
}
