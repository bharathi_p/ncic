﻿using System.Linq;
using DAL;
using NCICObjects;
using System.Collections.Generic;

namespace BAL
{
    public class CrimeAccount
    {
        public List<CrimeObject> GetPrisonersDetails(CrimeSearchObject oSearchObject)
        {
            NCICDataContext oNCICDataContext = new NCICDataContext();
            List<CrimeObject> oLstCrime = null;

            var oLstPrisonerDetails = (from oPrisoner in oNCICDataContext.Prisoners
                               join oCrimeDetails in oNCICDataContext.CrimeDetails on oPrisoner.ID equals oCrimeDetails.PrisonersId
                               join oGender in oNCICDataContext.Genders on oPrisoner.GenderId equals oGender.ID
                                join oNationalities in oNCICDataContext.Nationalities on oPrisoner.nationalityId equals oNationalities.ID
                              // where oLogin.UserName ==    
                               select new
                               {
                                   oPrisoner,
                                   oCrimeDetails,
                                   oGender.Sex,
                                   oNationalities.Nationality1

                               }).ToList();
            oLstCrime = new List<CrimeObject>();
            CrimeObject oCrime = new CrimeObject();
            foreach (var oCrimeObject in oLstPrisonerDetails)
            {
                oCrime = new CrimeObject();
                oCrime.Name = oCrimeObject.oPrisoner.Name;
                oCrime.Address = oCrimeObject.oPrisoner.Address;
                oCrime.DoB = oCrimeObject.oPrisoner.DoB;
                oCrime.Sex = oCrimeObject.Sex;
                oCrime.height = oCrimeObject.oPrisoner.height;
                oCrime.weight = oCrimeObject.oPrisoner.weight;
                oCrime.nationality = oCrimeObject.Nationality1;
                oCrime.CrimeType = oCrimeObject.oCrimeDetails.CrimeType;
                oCrime.CrimeDate = oCrimeObject.oCrimeDetails.CrimeDate;
                oCrime.Details = oCrimeObject.oCrimeDetails.Details;
                oLstCrime.Add(oCrime);
            }
            return oLstCrime;

        }
         

    }
}
