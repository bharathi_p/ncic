﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCICObjects
{
    [DataContract]
    public class LoginObject
    {
        [DataMember]
        public int ID { get; set; } 
        [DataMember]
        public string EmailId { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public string UserName { get; set; }
          [DataMember]
        public string Password { get; set; }


        
        [DataMember]
          public string ContactNo { get; set; }
        [DataMember]
        public DateTime CreatedAt { get; set; }
        [DataMember]
        public int CreatedBy { get; set; }
        [DataMember]
        public DateTime ModifiedAt { get; set; }
        [DataMember]
        public int ModifiedBy { get; set; }
        [DataMember]
        public string Status { get; set; }
    }
}
