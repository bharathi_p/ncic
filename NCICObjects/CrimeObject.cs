﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCICObjects
{
    [DataContract]
    public class CrimeObject
    { 
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public DateTime? DoB { get; set; }
        [DataMember]
        public string Sex { get; set; }
        [DataMember]
        public double? height { get; set; }
        [DataMember]
        public double? weight { get; set; }
        [DataMember]
        public string nationality { get; set; }
        [DataMember]
        public string CrimeType { get; set; }
        [DataMember]
        public DateTime? CrimeDate { get; set; }
        [DataMember]
        public string Details { get; set; }


    }
}
