﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCICObjects
{
    [DataContract]
    public class CrimeSearchObject
    {

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime DOBFrom { get; set; }
        [DataMember]
        public DateTime DOBTo { get; set; }
        [DataMember]
        public int? SexID { get; set; }
        [DataMember]
        public string Sex { get; set; }
        [DataMember]
        public float HeightFrom { get; set; }
        [DataMember]
        public float HeightTo { get; set; }
        [DataMember]
        public float weightFrom { get; set; }
        [DataMember]
        public float weightTo { get; set; }
        [DataMember]
        public int NationalityId { get; set; }
        [DataMember]
        public string Nationality { get; set; }
        
         
    }
}
