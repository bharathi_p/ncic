﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using NationalCriminalInformationCenter.Models;
using NationalCriminalInformationCenter.ServiceReference1;
using NCICObjects;


namespace NationalCriminalInformationCenter.Controllers
{
    public class HomeController : Controller
    {

        ServiceReference1.NCICServiceClient ncicService = new ServiceReference1.NCICServiceClient();

        //public ActionResult Product()
        //{
        //   // obj.CreateLogin();
        //}

        public ActionResult Index(string Email)
        {

            List<SelectListItem> Gender = new List<SelectListItem>();
            Gender.Add(new SelectListItem { Text = "Male", Value = "1" });
            Gender.Add(new SelectListItem { Text = "FeMale", Value = "0" });
            ViewBag.Gender = new SelectList(Gender, "Value", "Text", "Male");

            Session["Email"] = Email;
            ViewBag.DisplayEmail = Convert.ToString(Session["Email"]); 
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [AllowAnonymous]
        public ActionResult Search()
        {
          
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(SearchModel model)
        {
            DateTime today = DateTime.Today;
              
            LoginObject oLoginObject = new LoginObject();
            SearchObject oSearchObject = new SearchObject();
            oSearchObject.Name = model.Name;
            oSearchObject.DOBFrom = today.AddYears(-model.FromAge);
            oSearchObject.DOBTo = today.AddYears(-model.FromAge);
           //oSearchObject.SexID =model.SexId
            oSearchObject.Sex = model.Sex;
            oSearchObject.HeightFrom = model.FromHeight;
            oSearchObject.HeightTo = model.ToHeight;
            oSearchObject.weightFrom =model.FromWeight;
            oSearchObject.weightTo =model.ToWeight;
            oSearchObject.NationalityId =model.NationId;


             ncicService.GetPrisonersDetails(oSearchObject);
            
            return View(model);
        }
    }
}
