﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using NCICObjects;

namespace WcfEfMvcService
{
    ////Describes which operations the client can perform on the service
    [ServiceContract]
    public interface IProductService
    {
        
        [OperationContract]
        void CreateLogin(LoginObject oLoginObject);
       
    }  
}
