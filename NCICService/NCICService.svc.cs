﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BAL;
using NCICObjects;

namespace NCICService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class NCICService : INCICService
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public bool CreateLogin(LoginObject oLoginObject)
        {
            UserAccount oLoginAccount = new UserAccount();
            return oLoginAccount.CreateLogin(oLoginObject);
        }
        public LoginObject UserLogin(string EmailId, string Password)
        {
            UserAccount oLoginAccount = new UserAccount();
            return oLoginAccount.UserLoginDetails(EmailId, Password);
        }
        public List<CrimeObject> GetPrisonersDetails(CrimeSearchObject oSearchObject)
        {
            CrimeAccount oCrimeAccount = new CrimeAccount();
            return oCrimeAccount.GetPrisonersDetails(oSearchObject);
        }
    }
}
